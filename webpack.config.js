'use strict';

module.exports = {
    entry: "./assets/js/home",
    output: {
        path: __dirname + "/public",
        publicPath: "public/",
        filename: "all.js"
    }
};