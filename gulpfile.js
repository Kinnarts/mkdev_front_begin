'use strict';
var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var gutil = require("gulp-util");
var webpack = require("webpack");
var WebpackDevServer = require("webpack-dev-server");
var webpackConfig = require("./webpack.config.js");
var connect = require('gulp-connect');

gulp.task("prepare-css", function() {
    return gulp.src(['assets/css/bootstrap.css', 'assets/css/bootstrap-theme.css'])
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(concat('all.css'))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public'))
        .pipe(connect.reload());
});

var myDevConfig = Object.create(webpackConfig);
myDevConfig.devtool = "sourcemap";
myDevConfig.debug = true;
var devCompiler = webpack(myDevConfig);

gulp.task("webpack:build-dev", function(callback) {
    devCompiler.run(function(err, stats) {
        if(err) throw new gutil.PluginError("webpack:build-dev", err);
        gutil.log("[webpack:build-dev]", stats.toString({
            colors: true
        }));
        callback();
    });
});

gulp.task('connect', function() {
    connect.server({
        root: 'public',
        index: 'home.html',
        livereload: true
    });
});

gulp.task('default', gulp.series('webpack:build-dev', 'prepare-css', 'connect'));

gulp.watch("assets/**/*", gulp.series('webpack:build-dev', 'prepare-css'));
